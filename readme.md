#  Altran Challenge

Defines three **Centos 7 Server** running on AWS Platform

# Scope:

* ansible - configuration management to provisioning servers with playbooks
* terraform - deploy IaC the platform on AWS
* packer - Create the AMI (Alternative Provisioner Servers)

# Requirements:

* ansible
* terraform
* packer
* awscli
* git

# Usage

There are two folders one of these to create the infrastructure with terraform and another one to provisioning the servers with ansible. 

## Steps:

* run the terraform files
* get the internal IP address from nodes **Centos**
* seting up the **vars** missing on ansible playboooks
* run the playbooks for **nodes**
* verify the app listen on **9000** port on each **node**
* before run LB playbook, you need to set vars: IP node1 & node2 / put the certs into the folder: /etc/ssl/private/example.com.pem
* run the playbook for **lb** LoadBalancer
* in a browser, check with the **elasticIP** to access with   `https://ElasticIP`
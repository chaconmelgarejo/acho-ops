#!/bin/bash

sudo yum -y  update
sudo yum install -y java-1.8.0-openjdk-devel wget git ansible
sudo echo "JAVA_HOME=$(readlink -f /usr/bin/java | sed "s:bin/java::")" | sudo tee -a /etc/profile
source /etc/profile
sudo mkdir workspace
sudo cd workspace
sudo wget http://www-us.apache.org/dist/maven/maven-3/3.5.4/binaries/apache-maven-3.5.4-bin.tar.gz
sudo tar -xvzf apache-maven-3.5.4-bin.tar.gz
sudo mv apache-maven-3.5.4 /opt
sudo chown -R root:root /opt/apache-maven-3.5.4
sudo ln -s /opt/apache-maven-3.5.4 /opt/apache-maven
sudo echo 'export PATH=$PATH:/opt/apache-maven/bin' | sudo tee -a /etc/profile
source /etc/profile
# deploy app
sudo mkdir /home/centos
sudo cd /home/centos
sudo git clone https://github.com/spring-projects/spring-boot.git
sudo cd /home/centos/spring-boot/spring-boot-samples/spring-boot-sample-hateoas
sudo mvn compile
sudo mvn package
sudo mvn install
sudo mvn spring-boot:run -Dmaven.tomcat.port=9000 &
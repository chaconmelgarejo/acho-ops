#  Vagran Deploy

Defines three **Centos 7 Server** running on Vagrant/Vbox

# Requirements:

* vagrant
* virtualbox

# Usage

* Inside the Vagrant Folder:
```
$ vagrant up
...
```
* Provisioning nodes VM
```
$ vagrant provision
...
```
* to access to any node and lb
```
$ vagrant ssh node1
...
```
#   Deployment with ansible - Altran Project

Defines three **Centos 7 Server** running on AWS Platform

# Requirements

* ansible
* git
* wget
* set vars on lb.yml (IP nodes & ssl cert)

# Pre-Run Playbooks

we need to add new lines in `/etc/hosts/` and inventory `/etc/ansible/hosts` :

* in `/etc/hosts/` 
```
127.0.0.1		centos
```

* in `/etc/ansible/hosts`

```
centos
```


# Run playbook on nodes

```
ansible-playbook nodes.yml
```

# Run Playbook on LoadBalancer - lb
```
ansible-playbook lb.yml
```

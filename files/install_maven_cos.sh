#!/bin/bash

yum -y  update
yum install -y java-1.8.0-openjdk-devel wget git ansible
echo "JAVA_HOME=$(readlink -f /usr/bin/java | sed "s:bin/java::")" | sudo tee -a /etc/profile
source /etc/profile
mkdir workspace
cd workspace
wget http://www-us.apache.org/dist/maven/maven-3/3.5.4/binaries/apache-maven-3.5.4-bin.tar.gz
tar -xvzf apache-maven-3.5.4-bin.tar.gz
mv apache-maven-3.5.4 /opt
chown -R root:root /opt/apache-maven-3.5.4
ln -s /opt/apache-maven-3.5.4 /opt/apache-maven
echo 'export PATH=$PATH:/opt/apache-maven/bin' | sudo tee -a /etc/profile
source /etc/profile
# deploy app
cd /home/centos
git clone https://github.com/spring-projects/spring-boot.git
cd /home/centos/spring-boot/spring-boot-samples/spring-boot-sample-hateoas
mvn compile
mvn package
mvn install
mvn spring-boot:run -Dmaven.tomcat.port=9000 &
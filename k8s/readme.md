#  Altran Challenge on K8s

Deploy the spring-boot / Java app i Kubernetes

# Requirements:

* Built with maven and save in the registry/artifacory storage: `spring-boot-sample-hateoas-0.0.1-SNAPSHOT.jar`
* Spin up a K8s cluster ( GCP (terraform) / AWS (kops/terraform) / Minikube (Test Enviroment))
* Configure or have one Registry Server to save (push) the docker image `$CLOUD_REGISTRY/spring-boot-hateoas`
 
# Usage:

## Steps:

* Clone the repo from github
* Built the app with Maven, resulting the `.jar` File locate `target` folder
* Built the Docker Container Image `Dockerfile`
* Save the Image in the registry (DockerHub / Jfrog- Artifactory / Local registry)

Example using DockerHub:
```
$ mvn clean install
…
$ docker build -t achops/spring-boot-hateoas:1.0 .
...
$ docker push achops/spring-boot-hateoas:1.0
...
```


On K8s Cluster:
* Apply `services.yaml` to expose port and start a service type `NodePort`
* Deploy the app with `deployment-hateoas.yaml`

```
$ kubectl apply -f service.yaml
service "spring-boot-hateoas-service" created

$ kubectl apply -f deployment-hateoas.yaml
service "spring-boot-hateoas-deploy" created
```

# Alternatives:

* we could use another alternative tools like Fabric8 Plugin (Maven/ K8s), so we need to get a deep research in that world...
  https://fabric8.io/guide/mavenPlugin.html
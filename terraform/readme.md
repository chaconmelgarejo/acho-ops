﻿
#   Infrastructure Altran Project with Packer and Terraform

Defines three **Centos 7 Server** running on AWS Platform

# Requirements

* awscli
* terraform
* packer

# Pre-Build Packer

we need to set on `ami-cos.json` these vars:

* vpc_id
* subnet_id
* security_group_id

# Build images with Packer

a packer builder template to create the AMI during provision with terraform;  `ami-cos.json` Create or update the AMIs running: 


```
packer build ami-cos.json
```

# Create infrastructure with Terraform

## Initialize

Initializes backend and downloads modules

```
terraform init
```

## Plan

Refresh status and calculate actions to update the infrastructure

```
terraform plan
```

## Apply

Deploy changes on the actual infrastructure

```
terraform apply


variable "aws_access_key" {
  default = ""
}
variable "aws_secret_key" {
  default = ""
}
variable "region" {
  default = "us-east-1"
}

variable "cos_ami" {
  default = "ami-9887c6e7"
}

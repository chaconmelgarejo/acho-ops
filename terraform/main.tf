provider "aws" {

  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region = "${var.region}"
  profile   = "altran-prod"
}

data "terraform_remote_state" "altran-prod" {
  backend = "s3"
  config {
    bucket = "altran-tf"
    key    = "altran-prod.tfstate"
    region = "us-east-1"
  }
}

module "altran_vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "altran-vpc"
  cidr = "10.0.0.0/16"

  azs             = ["us-east-1a"]
  public_subnets = ["10.0.1.0/24", "10.0.2.0/24"]
    
  enable_dns_hostnames = true

  tags = {
    Terraform = "true"
    Environment = "prod"
  }
}

module "altran_sg" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "altran-sg"
  description = "Security group altran app"
  vpc_id      = "${module.altran_vpc.vpc_id}"

  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["http-80-tcp", "https-443-tcp", "ssh-tcp"]

  ingress_with_cidr_blocks = [
    {
      from_port   = 9000
      to_port     = 9000
      protocol    = "tcp"
      description = "altran-service port"
    } ]

  egress_rules        = ["all-all"]
}

module "nodo1_ec2" {
  source = "terraform-aws-modules/ec2-instance/aws"

  name                        = "nodo1"
  ami                         = "${var.cos_ami}"
  instance_type               = "t2.micro"
  key_name                    = "cos-ops"
  associate_public_ip_address = "true"
  subnet_id                   = "${element(module.altran_vpc.public_subnets, 0)}"
  vpc_security_group_ids      = ["${module.altran_sg.this_security_group_id}"]
  }

module "nodo2_ec2" {
  source = "terraform-aws-modules/ec2-instance/aws"

  name                        = "nodo2"
  ami                         = "${var.cos_ami}"
  instance_type               = "t2.micro"
  key_name                    = "cos-ops"
  associate_public_ip_address = "true"
  subnet_id                   = "${element(module.altran_vpc.public_subnets, 0)}"
  vpc_security_group_ids      = ["${module.altran_sg.this_security_group_id}"]
 }

module "lb_ec2" {
  source = "terraform-aws-modules/ec2-instance/aws"

  name                        = "lb"
  ami                         = "${var.cos_ami}"
  instance_type               = "t2.micro"
  key_name                    = "cos-ops"
  subnet_id                   = "${element(module.altran_vpc.public_subnets, 0)}"
  vpc_security_group_ids      = ["${module.altran_sg.this_security_group_id}"]
  associate_public_ip_address = true

}

resource "aws_eip" "lb_eip" {
  instance = "${element(module.lb_ec2.id, 0)}"
  vpc      = true
}